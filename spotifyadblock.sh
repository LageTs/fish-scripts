spotify 1>/dev/null 2>&1 &
set playing true
set idle true
set waiting true
while true
	if $waiting
		set windownumbers (xdotool search -classname spotify)
	end
	for windownumber in $windownumbers
		if xtitle $windownumber | grep -qs "Spotify Free"
			if $idle
				echo "Idle"
				set idle false
				set playing true
				set windownumbers $windownumber
				set waiting false
			end
		else if xtitle $windownumber | grep -qs "-"
			if $playing
				echo "Playing"
				set playing false
				set idle true
				set windownumbers $windownumber
				set waiting false
			end
		else if xtitle $windownumber | grep -qs "Advertisement"
			set playing true
			set idle true
			set waiting true
			echo "Ads"
			xdotool windowclose $windownumber
			sleep 1
			spotify 1>/dev/null 2>&1 &
			sleep 3
			playerctl play
		else
			echo "Searching for correct Window"
		end
	end
	sleep 1
end
